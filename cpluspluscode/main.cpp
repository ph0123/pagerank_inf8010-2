//
//  main.cpp
//  pagerank
//
//  Created by cnphuong on 3/17/20.
//  Copyright © 2020 cnphuong. All rights reserved.
//  This code is used some part of Saddlebags framework.

#include <chrono>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <iostream>
#include <sched.h>
#include <sstream>
#include <string>
#include <unistd.h>
#include <vector>
#include <sched.h>
#include <unordered_map>

using namespace std;


unordered_map<int, int> map_list;//id,index in vertex list
//define values
#define INITIAL_RESERVE_SIZE_MAX_EDGES 50
#define SEP ','
#define EXPORT_RESULT false
#define DEBUG_DETAILED false
//end define


// Get File Name from a Path with or without extension
std::string getFileName(std::string filePath, bool withExtension = true, char seperator = '/')
{
    // Get last dot position
    std::size_t dotPos = filePath.rfind('.');
    std::size_t sepPos = filePath.rfind(seperator);

    if(sepPos != std::string::npos) {
        return filePath.substr(sepPos + 1, filePath.size() - (withExtension || dotPos != std::string::npos ? 1 : dotPos) );
    }

    return "";
}

// Print worker information
std::string get_worker_hello() {

    std::ostringstream s;
    char hostname[256];
    gethostname(hostname, sizeof(hostname));
//    int cpu = -1;
//    cpu = sched_getcpu(); not work on Xcodes uncomment when use saga compiler.
//    s << "[Worker " << husky::Context::get_global_tid() << "] "
    s << " Node "<< hostname << ".";
//      << " CPU " << cpu << ".";

    return s.str();
}



// Print PageRank information for all vertices
template<typename V>
std::string get_all_pagerank(V& vertex_list, int iter = 0, int interval = 5) {

    if (interval <= 0)
        return "";

    interval = (int) vertex_list.get_size() / interval;

    if (interval <= 0)
        return "";

    std::ostringstream s;
    s << std::endl;

    for (int i = 1; i < vertex_list.get_size(); i += interval) {
        auto u = vertex_list.get(i);

        s << "[Iter " << iter << "]"
          << " ID: " << u.id()
          << ", PageRank: " << u.pr
          << ", Edges: " << u.adj.size()
          << std::endl;
    }

    if (EXPORT_RESULT) {
        // Format
        std::cout << "proc id,iteration,vertex id,num neighbors,page rank" << std::endl;

        //need to edit --worker ID if run with openmpi.
        //int proc_id = husky::Context::get_global_tid();

        for (int i = 0; i < vertex_list.get_size(); i++) {
            auto u = vertex_list.get(i);

            //std::cout << proc_id << SEP
               std::cout << iter << SEP
                         << u.id() << SEP
                         << u.adj.size() << SEP
                         << u.pr
                         << std::endl;
        }
    }

    return s.str();
}

class Vertex {
   public:
    using KeyT = int;
    int vertexId;
    std::vector<int> links;
    std::vector<int> in_links;
    float pr=1;
    float new_pr =0;

    
    Vertex(){}
    explicit Vertex(const KeyT& id) : vertexId(id), pr(0.15) {}
    const KeyT& id() const { return vertexId; }

    // Serialization and deserialization
    void add_link(int new_link)
    {
        // TODO: Minor, check just in case this link already exists
        this->links.emplace_back(new_link);
    }
    void on_push_recv(double val)
    {
        new_pr += val;
    }

    void finishing_work()
    {
        pr = new_pr;
        new_pr = 0;
    }
    
};

int load_data(std::string data_file,
              int& total_vertices, int& total_edges, int& total_rows, int& rows_skipped, vector <Vertex> &vertex_list)
{
    bool isRankRoot=true;
    //bool isRankRoot = upcxx::rank_me() == 0;
    int rank_me = 0;//saddlebags::rank_me();
    //int rank_n = 1;//upcxx::rank_n();
    std::string line;
    std::string vertex_str;
    //int prog_counter = 0;
    int format_neighbor_start_index = 2;
    int neighbor;

    auto start_time = std::chrono::high_resolution_clock::now();

    if(isRankRoot && DEBUG) {
        std::cout << "Loading data from: " << data_file << std::endl;
    }

    std::ifstream infile(data_file);
    if (! infile.good()) {
        //if(upcxx::rank_me() == 0) {
            std::cerr << "ERROR: Unable to open file: " << data_file << std::endl;
        //}
        return -1;
    }

    cout<<"start reading ..."<<endl;
    while (std::getline(infile, line))
    {

            if (line.empty()) {
                rows_skipped++;
                continue;
            }

            std::stringstream ss(line);
            std::vector <std::string> tokens;

            while (ss >> vertex_str) {
                tokens.push_back(vertex_str);
            }

            if (tokens.size() < 2) {
                std::cerr << "[Rank " << rank_me << "] ERROR: Unable to parse vertex for: " << line << std::endl;
                rows_skipped++;
                continue;
            }

            // Possible graph file formats like: source num_neighbors neighbor_1 neighbor_2 ... neighbor_n
            if (tokens.size() >= 3) {
                format_neighbor_start_index = 2;
            } else {
                format_neighbor_start_index = 1;
            }

            int vertex = stoi(tokens[0]);
            total_rows++;

            bool is_my_obj = true;// (worker->get_partition(VERTEX_TABLE, vertex) == rank_me);

            if (is_my_obj)
            {
                Vertex *new_obj = new Vertex();
                new_obj->vertexId =vertex;
                total_vertices++;

                if(new_obj!=NULL)
                {
                    for(int i = format_neighbor_start_index; i < tokens.size(); i++)
                    {
                        neighbor = stoi(tokens[i]);
                        new_obj->links.emplace_back(neighbor);
                        total_edges++;
                    }
                    vertex_list.emplace_back(*new_obj);
                    map_list.insert({vertex,total_rows-1});
    //                cout<<total_edges<<endl;
    //
    //                if (total_vertices == 1 && DEBUG_DETAILED)
    //                {
    //                    std::cout << "[Rank " << rank_me << "] Inserted first vertex <" << vertex << ">."
    //                        << std::endl;
    //                }
                }
            }
            //if(total_rows%100000==0) cout<<total_rows<<endl;
        }

        infile.close();
    //update out_links for all
    std::ostringstream s_in;
    for(auto it : vertex_list)
    {
        s_in<<"check inlinks"<<it.vertexId<<endl;
        for(int i=0; i< it.links.size(); i++)
        {
            int dest = it.links[i];
            s_in<<dest<<",";
            int source = it.vertexId;
            int index = map_list.find(dest)->second;
            s_in<<"is stored at "<<index<<",";
            vertex_list[index].in_links.emplace_back(source);
        }
        cout<<s_in.str()<<endl;
    }
    for(auto it : vertex_list)
    {
        cout<<it.in_links.size()<<endl;
    }
    cout<<"ok2"<<endl;
        if(DEBUG_DETAILED)
        {
            auto timenow = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
            std::cout << "[Rank " << rank_me << "] "
                      << ctime(&timenow)
                      << "Inserted objects: " << total_vertices
                      << " (Out of total objects: " << total_rows << ")" << std::endl;
        }

        //upcxx::barrier();
        auto end_data_gen = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double> elapsed_time_data_gen = end_data_gen - start_time;
        double duration_data_gen = elapsed_time_data_gen.count() * 1e+3;

        if (DEBUG && isRankRoot)
        {
            
            auto timenow = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
            std::cout << "[Rank " << rank_me << "] "
                      << ctime(&timenow)
                      << "Input file loaded with " << total_rows
                      << " objects in " << duration_data_gen << " ms." << std::endl;
        }
    return 0;
}
void pagerank()
{
    //auto start_time = std::chrono::high_resolution_clock::now();
    
}
//link to data /Users/cnphuong/Library/Developer/Xcode/DerivedData/pagerank-ctedtqhqfghptjfsulbzeamxoujj/Build/Products

// Print maximum PageRank information
std::string get_max_pagerank(vector<Vertex>& vertex_list, int iter = 0, bool detailed = true) {

    cout<<"ok 3"<<endl;
    std::ostringstream s;
    double max_pr = 0;
    int max_pr_id = 0;

    // Iterate over all vertex objects
    for (int i = 0; i < vertex_list.size(); i++) {
        auto u = vertex_list[i];
        if (u.pr >= max_pr) {
            max_pr = u.pr;
            max_pr_id = u.vertexId;
        }
    }

    if (detailed) {
        s << std::endl
          << "[Iter " << iter << "]"
          << " Max ID: " << max_pr_id
          << ", Max PageRank:" << max_pr;
    } else {
        s << max_pr << SEP << max_pr_id;
    }

    return s.str();
}

int main(int argc, char* argv[])
{
    std::string data_file = "web-indochina.mtx";
    int iterations = 3;
    int max_size = 10000;

    if (argc > 1) {
        data_file = argv[1];
    } else {
        std::cerr << "Usage: " << argv[0] << " <Path> <Iterations> <Buffer Size>" << std::endl;
    }

    if (argc > 2) {
        iterations = atoi(argv[2]);
    }

    if (argc > 3) {
        max_size = atoi(argv[3]);
    }

    //parallel here
    int total_vertices = 0;
    int total_edges = 0;
    int total_rows = 0;
    int rows_skipped = 0;
    vector<Vertex> Vertex_List; // partition table. - each worker will have one or more table. (in this case, I use 1 b/c this is a serial program)
    data_file = "my_data2.txt";
    load_data(data_file, total_vertices, total_edges, total_rows, rows_skipped,Vertex_List);
    cout<<Vertex_List.size()<<endl;

    
    iterations=3;
    //do all iterations for pagerank: initial pr=1, new_pr=0
    std::ostringstream s;
    for(int iter=0; iter<iterations; iter++)
    {
        for(int element=0; element<Vertex_List.size(); element++)
        {
            if (Vertex_List[element].links.size() == 0)
                                continue;
            cout<<Vertex_List[element].links.size()<<endl;
            if (iter > 0)
            {
                cout<<"phuong check ID: "<<Vertex_List[element].vertexId<<" "<<endl;
                double pr_neighbor = 0.0;
                for(auto in_vertex : Vertex_List[element].in_links)
                {
                    //cout<<" " <<in_vertex;
                    int index = map_list.find(in_vertex)->second;
                    pr_neighbor += Vertex_List[index].pr/Vertex_List[index].links.size();
                }
                Vertex_List[element].new_pr = 0.85 * pr_neighbor + 0.15;
                cout<<"check new_pr: "<<Vertex_List[element].vertexId<<" "<< Vertex_List[element].new_pr<<endl;
                cout<<endl;
            }
        }
        if (iter > 0)
        {
            for(int element=0; element<Vertex_List.size(); element++)
            {
                Vertex_List[element].pr= Vertex_List[element].new_pr;
                Vertex_List[element].new_pr=0.0;
                //s<<" "<<Vertex_List[element].pr; // check for pagerank value
            }
            //s<<endl;
        }



    }
    cout<<s.str()<<endl;
    
    cout<<"pagerank after run: ";
    for(auto element : Vertex_List)
    {
        cout<<element.pr<<" ";
    }
    cout<<endl;
    cout<<get_max_pagerank(Vertex_List, iterations);

    cout<<"done"<<endl;
    return 0;
}
