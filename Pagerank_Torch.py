import time  # good version for
import numpy as np
import socket
import cpuinfo
import time
import datetime
import sys
import os
import torch
import torch.distributed as dist
from torch.multiprocessing import Process
import torch.distributed.rpc as rpc
import numpy as np
# <int, int> map_list; in python I use dictionary, map from index to object Vertex.
# define INITIAL_RESERVE_SIZE_MAX_EDGES 50
# define SEP ','
# define EXPORT_RESULT false
# define DEBUG_DETAILED false
INITIAL_RESERVE_SIZE_MAX_EDGES = 50
SEP = '\t'
EXPORT_RESULT = False
DEBUG_DETAILED = False
DEBUG = False

_local_dict = {}


def getFileName(filePath, withExtension=True, seperator='/'):
    dotPos = filePath.rfind('.')
    sepPos = filePath.rfind(seperator);
    if (sepPos != -1 and dotPos != -1 and withExtension == True):
        return filePath[sepPos + 1:]
    if (withExtension == False):
        return filePath[sepPos + 1:]
    return ""


# Print worker information
def get_worker_hello():
    hostname = socket.gethostname()
    cpu = cpuinfo.get_cpu_info()['brand']
    s = "Node: " + hostname + ".\n" + "CPU: " + cpu + ".";
    return s


class Vertex:
    def __init__(self, index):
        self.vertexId = index
        self.links = []
        self.in_links = []
        self.pr = 0.15
        self.new_pr = 0.0

    def add_link(new_link):
        self.links.append(new_link)

    def on_push_recv(val):
        self.new_pr += val;

    def finishing_work():
        self.pr = self.new_pr;
        self.new_pr = 0.0;

    def __str__(self):
        return "ID" + str(self.vertexId) + " " + str(self.pr) + " from object Vertex"


# line 66.


def load_data(data_file):
    total_vertices = 0
    total_edges = 0
    total_rows = 0
    rows_skipped = 0
    # return all value without data_file
    isRankRoot = True
    # bool isRankRoot = upcxx::rank_me() == 0;
    rank_me = 0;  # saddlebags::rank_me();
    # int rank_n = 1;#upcxx::rank_n();
    line = ""
    vertex_str = "";
    # int prog_counter = 0;
    format_neighbor_start_index = 2
    neighbor = -1
    DEBUG = False
    start_time = time.perf_counter()

    if (isRankRoot == True) and (DEBUG == True):
        print("Loading data from: " + str(data_file))

    print("start reading ...")
    file1 = open(data_file, 'r')
    Lines = file1.readlines()
    for line in Lines:
        if line == "\n":  # empty line
            rows_skipped += 1
            continue
        tokens = line.split(" ")

        if (len(tokens) < 2):
            print("[Rank " + str(rank_me) + "] ERROR: Unable to parse vertex for: " + str(line))
            rows_skipped += 1
            continue
        # Possible graph file formats like: source num_neighbors neighbor_1 neighbor_2 ... neighbor_n
        if (len(tokens) >= 3):
            format_neighbor_start_index = 2
        else:
            format_neighbor_start_index = 1
        vertex = int(tokens[0])
        total_rows += 1
        is_my_obj = True  # (worker->get_partition(VERTEX_TABLE, vertex) == rank_me);
        if (is_my_obj == True):
            new_obj = Vertex(vertex)
            total_vertices += 1
            for i in range(format_neighbor_start_index, len(tokens)):
                neighbor = int(tokens[i]);
                new_obj.links.append(neighbor)
                total_edges += 1
            _local_dict[vertex] = new_obj
        if (total_vertices == 1 and DEBUG_DETAILED == True):
            print("[Rank " + str(rank_me) + "] Inserted first vertex <" + str(vertex) + ">.")
        # if(total_rows%100000==0) cout<<total_rows<<endl;
    file1.close()

    # update out_links for all
    # std::ostringstream s_in;
    s_in = ""
    for it in _local_dict:
        u = _local_dict[it]
        s_in += "check inlinks" + str(u.vertexId)
        for i in range(0, len(u.links)):
            dest = int(u.links[i]);
            s_in += str(dest) + ","
            source = u.vertexId
            # need to call rpc to dest worker to do it.  //parallel here
            _local_dict[dest].in_links.append(
                source)  # need to check with saddlebags because dest does not belong to this worker

        # print(s_in)

    # for it in _local_dict:
    #    print(str(len(_local_dict[it].in_links)))
    if (DEBUG_DETAILED == True):
        timenow = datetime.datetime.now()
        print("[Rank " + str(rank_me) + "] "
              + str(timenow)
              + "Inserted objects: " + str(total_vertices)
              + " (Out of total objects: " + str(total_rows) + ")")
    # upcxx::barrier();
    end_data_gen = time.perf_counter()
    elapsed_time_data_gen = (end_data_gen - start_time) * 1000  # milisecond

    if (DEBUG == True and isRankRoot == True):
        timenow = datetime.datetime.now()
        print("[Rank ", rank_me, "] "
              , timenow
              , "Input file loaded with ", total_rows
              , " objects in ", duration_data_gen, " ms.")
    return total_vertices, total_edges, total_rows, rows_skipped


def get_max_pagerank(iter=0, detailed=True):
    s = ""
    max_pr = 0.0
    max_pr_id = 0
    # Iterate over all vertex objects
    for it in _local_dict:
        u = _local_dict[it]
        if (u.pr >= max_pr):
            max_pr = u.pr;
            max_pr_id = u.vertexId;
    if (detailed == True):
        s = s + "\n" + "[Iter " + str(iter) + "]" + " Max ID: " + str(max_pr_id) + ", Max PageRank:" + str(max_pr)
    else:
        s = s + str(max_pr) + str(SEP) + str(max_pr_id)
    return s


def run():
    # for arg in sys.argv[1:]:
    #    print (arg)
    # print(sys.argv)
    data_file = "simple_graph.txt" #"/Users/cnphuong/python_code/simple_graph.txt"  # sys.argv[1]
    iterations = 10
    max_size = 10000

    # if (len(sys.argv[3:]) > 1):
    #    data_file = sys.argv[1]
    # else:
    #    print("Usage: " , sys.argv[0] , " <Path> <Iterations> <Buffer Size>")

    # if (len(sys.argv[3:])  > 2):
    #     iterations = int(sys.argv[2])

    # if (argc > 3):
    #     max_size = atoi(sys.argv[3]);

    print(data_file)
    print(iterations)
    print(max_size)

    # parallel here
    total_vertices = 0
    total_edges = 0
    total_rows = 0
    rows_skipped = 0
    # vector<Vertex> Vertex_List; # partition table. - each worker will have seperated _local_dict(in this case, I use 1 b/c this is a serial program)
    total_vertices, total_edges, total_rows, rows_skipped = load_data(data_file)
    # print(len(_local_dict))

    # do all iterations for pagerank: initial pr=1, new_pr=0
    #warm up 1 iteration.
    s = ""
    for element in _local_dict:
        if len(_local_dict) == 0:
            continue
        # print(len(_local_dict[element].links))
        pr_neighbor = 0.0;
        for in_vertex in _local_dict[element].in_links:
                # print(in_vertex)#needto check
            pr_neighbor += _local_dict[in_vertex].pr / len(_local_dict[in_vertex].links)
        _local_dict[element].new_pr = 0.85 * pr_neighbor + 0.15
            # print("check new_pr: ",_local_dict[element].vertexId," ", _local_dict[element].new_pr)

    for element in _local_dict:
        _local_dict[element].pr = _local_dict[element].new_pr
        _local_dict[element].new_pr = 0.0
    s += " " + str(_local_dict[element].pr)  # check for pagerank value

    #end warm-up and run n iterations.
    s= ""
    for iter in range(0, iterations):
        for element in _local_dict:
            if len(_local_dict) == 0:
                continue
            # print(len(_local_dict[element].links))
            if (iter > 0):
                # print("phuong check ID: ", _local_dict[element].vertexId)
                pr_neighbor = 0.0;
                for in_vertex in _local_dict[element].in_links:
                    # print(in_vertex)#needto check
                    pr_neighbor += _local_dict[in_vertex].pr / len(_local_dict[in_vertex].links)
                _local_dict[element].new_pr = 0.85 * pr_neighbor + 0.15
                # print("check new_pr: ",_local_dict[element].vertexId," ", _local_dict[element].new_pr)

        if (iter > 0):
            for element in _local_dict:
                _local_dict[element].pr = _local_dict[element].new_pr
                _local_dict[element].new_pr = 0.0
                s += " " + str(_local_dict[element].pr)  # check for pagerank value
    # print(s)
    print("pagerank after run: ")
    # for element in _local_dict:
    # print(_local_dict[element].pr," ")

    print(get_max_pagerank(iterations))

# Edit here... to run with multiple CPUs
def init_process(rank, size, fn, backend='gloo'):
    """ Initialize the distributed environment. """
    os.environ['MASTER_ADDR'] = '127.0.0.1'
    os.environ['MASTER_PORT'] = '29500'

    my_name = "worker" + str(rank)
    my_target = "worker" + str((rank + 1) % size)

    # load data here

    _local_dict.update(create_dict(rank, size))
    my_key = [1, 2, 4, 3]
    #store data to _local_dict

    rpc.init_rpc(my_name, rank=rank, world_size=size)  # initial_rpc

    #work here instead of for loop.
    for k in range(0, 2):  # repeat many time :D
        my_result = 0
        for i in range(0, size):
            my_target = "worker" + str(i)
            results = run2(my_target, my_key)
            my_result += results
            print_hello(results, i, rank)  # call rank i print hello results and current rank.

    rpc.shutdown()  # end_rpc
    print(my_result)
    print(_local_dict, " from rank ", rank)


if __name__ == "__main__":
    size = 1
    processes = []
    for rank in range(size):
        p = Process(target=init_process, args=(rank, size, run))
        p.start()
        processes.append(p)

    for p in processes:
        p.join()
