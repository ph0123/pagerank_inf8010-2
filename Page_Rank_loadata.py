
import time  # good version for
import numpy as np
import socket
import cpuinfo
import time
import datetime
import sys
import os
import torch
import torch.distributed as dist
from torch.multiprocessing import Process
import torch.distributed.rpc as rpc
import numpy as np

import time  # good version for
import numpy as np
import socket
import cpuinfo
import time
import datetime
import sys

# <int, int> map_list; in python I use dictionary, map from index to object Vertex.
# define INITIAL_RESERVE_SIZE_MAX_EDGES 50
# define SEP ','
# define EXPORT_RESULT false
# define DEBUG_DETAILED false
INITIAL_RESERVE_SIZE_MAX_EDGES = 50
SEP = '\t'
EXPORT_RESULT = False
DEBUG_DETAILED = False
DEBUG = False

# <int, int> map_list; in python I use dictionary, map from index to object Vertex.
# define INITIAL_RESERVE_SIZE_MAX_EDGES 50
# define SEP ','
# define EXPORT_RESULT false
# define DEBUG_DETAILED false
INITIAL_RESERVE_SIZE_MAX_EDGES = 50
SEP = ','
EXPORT_RESULT = False
DEBUG_DETAILED = False
DEBUG = False

_local_dict = {}



"""RPC with Torch"""  # version sum some values from their key. get values from all workers (including current rank)
"""run.py:"""




def intersect_dict(dict1):
    ret = {}
    for key in _local_dict:
        if key in dict1:
            ret[key] = dict1[key]
    return ret


def intersect_dict2(target_keys):
    res = 0.0
    for key in target_keys:
        _key = int(key)
        if _key in _local_dict:
            res += _local_dict[_key]
    return res


def run(dst, dict1):
    intersect = rpc.rpc_sync(dst, intersect_dict, args=(_local_dict,))
    return intersect


def run2(dst, target_keys):
    intersect = rpc.rpc_sync(dst, intersect_dict2, args=(target_keys,))
    return intersect


def create_dict(rank, size):
    local_dict = {}
    for i in range(0, 10):
        if (i % size == rank):
            local_dict[i] = 1.2 * (rank + 1)
    return local_dict


def print_hello(results, current_rank, rank):
    print(results, ' on rank ', rank, " from rank ", current_rank)

 #FUNCTION OF PAGERANK
def get_worker_hello():
    hostname = socket.gethostname()
    cpu = cpuinfo.get_cpu_info()['brand']
    s = "Node: " + hostname + ".\n" + "CPU: " + cpu + ".";
    return s

def getFileName(filePath, withExtension=True, seperator='/'):
    dotPos = filePath.rfind('.')
    sepPos = filePath.rfind(seperator);
    if (sepPos != -1 and dotPos != -1 and withExtension == True):
        return filePath[sepPos + 1:]
    if (withExtension == False):
        return filePath[sepPos + 1:]
    return ""

# Print worker information
def get_worker_hello():
    hostname = socket.gethostname()
    cpu = cpuinfo.get_cpu_info()['brand']
    s = "Node: " + hostname + ".\n" + "CPU: " + cpu + ".";
    return s

class Vertex:
    def __init__(self, index):
        self.vertexId = index
        self.links = []
        self.in_links = []
        self.pr = 0.15
        self.new_pr = 0.0

    def add_link(new_link):
        self.links.append(new_link)

    def on_push_recv(val):
        self.new_pr += val;

    def finishing_work():
        self.pr = self.new_pr;
        self.new_pr = 0.0;

    def __str__(self):
        return "ID" + str(self.vertexId) + " " + str(self.pr) + " from object Vertex"

def load_data(data_file,my_rank, world_size):
    total_vertices = 0
    total_edges = 0
    total_rows = 0
    rows_skipped = 0
    # return all value without data_file
    isRankRoot = (my_rank==0)
    # bool isRankRoot = upcxx::rank_me() == 0;
    rank_me = my_rank  # saddlebags::rank_me();
    # int rank_n = 1;#upcxx::rank_n();
    line = ""
    vertex_str = "";
    # int prog_counter = 0;
    format_neighbor_start_index = 2
    neighbor = -1
    DEBUG = False
    start_time = time.perf_counter()

    if (isRankRoot == True) and (DEBUG == True):
        print("Loading data from: " + str(data_file))

    print("start reading ...")
    file1 = open(data_file, 'r')
    Lines = file1.readlines()
    for line in Lines:
        if line == "\n":  # empty line
            rows_skipped += 1
            continue
        tokens = line.split(" ")

        if (len(tokens) < 2):
            print("[Rank " + str(rank_me) + "] ERROR: Unable to parse vertex for: " + str(line))
            rows_skipped += 1
            continue
        # Possible graph file formats like: source num_neighbors neighbor_1 neighbor_2 ... neighbor_n
        if (len(tokens) >= 3):
            format_neighbor_start_index = 2
        else:
            format_neighbor_start_index = 1
        vertex = int(tokens[0])
        total_rows += 1
        is_my_obj = ((vertex%world_size) == rank_me)
        if (is_my_obj == True):
            new_obj = Vertex(vertex)
            total_vertices += 1
            for i in range(format_neighbor_start_index, len(tokens)):
                neighbor = int(tokens[i]);
                new_obj.links.append(neighbor)
                total_edges += 1
            _local_dict[vertex] = new_obj
        if (total_vertices == 1 and DEBUG_DETAILED == True):
            print("[Rank " + str(rank_me) + "] Inserted first vertex <" + str(vertex) + ">.")
        # if(total_rows%100000==0) cout<<total_rows<<endl;
    file1.close()



    # for it in _local_dict:
    #    print(str(len(_local_dict[it].in_links)))
    if (DEBUG_DETAILED == True):
        timenow = datetime.datetime.now()
        print("[Rank " + str(rank_me) + "] "
              + str(timenow)
              + "Inserted objects: " + str(total_vertices)
              + " (Out of total objects: " + str(total_rows) + ")")
    # upcxx::barrier();
    end_data_gen = time.perf_counter()
    elapsed_time_data_gen = (end_data_gen - start_time) * 1000  # milisecond

    if (DEBUG == True and isRankRoot == True):
        timenow = datetime.datetime.now()
        print("[Rank ", rank_me, "] "
              , timenow
              , "Input file loaded with ", total_rows
              , " objects in ", duration_data_gen, " ms.")
    return total_vertices, total_edges, total_rows, rows_skipped


def get_max_pagerank(iter=0, detailed=True):
    s = ""
    max_pr = 0.0
    max_pr_id = 0
    # Iterate over all vertex objects
    for it in _local_dict:
        u = _local_dict[it]
        if (u.pr >= max_pr):
            max_pr = u.pr;
            max_pr_id = u.vertexId;
    if (detailed == True):
        s = s + "\n" + "[Iter " + str(iter) + "]" + " Max ID: " + str(max_pr_id) + ", Max PageRank:" + str(max_pr)
    else:
        s = s + str(max_pr) + str(SEP) + str(max_pr_id)
    return s

class Edge:
    def __init__(self, source, dest):
        self.source= source
        self.dest = dest



def add_outlinks(arr):
    for ele in arr:
        dest = ele.dest
        source = ele.source
        if dest in _local_dict:
            _local_dict[dest].in_links.append(source)
def init_process(rank, size, fn, backend='gloo'):
    """ Initialize the distributed environment. """
    os.environ['MASTER_ADDR'] = '127.0.0.1'
    os.environ['MASTER_PORT'] = '29500'

    my_name = "worker" + str(rank)
    my_target = "worker" + str((rank + 1) % size)

    #_local_dict.update(create_dict(rank, size))
    #my_key = [1, 2, 4, 3]
    #LOAD DATA HERE
    data_file = "simple_graph.txt"  # "/Users/cnphuong/python_code/simple_graph.txt"  # sys.argv[1]
    iterations = 10
    max_size = 10000
    print(data_file)
    print(iterations)
    print(max_size)

    total_vertices = 0
    total_edges = 0
    total_rows = 0
    rows_skipped = 0

    #LOAD DATA
    total_vertices, total_edges, total_rows, rows_skipped = load_data(data_file,rank, size)
    #END LOAD DATA, IT WILL STORE TO _local_dict
    rpc.init_rpc(my_name, rank=rank, world_size=size)  # initial_rpc

    #CALL rpc TO OTHER RANKS

    # std::ostringstream s_in;
    s_in = ""


    for it in _local_dict:

        arr_send = []
        for i in range(0, size):
            temp = []
            arr_send.append(temp)

        u = _local_dict[it]
        source = u.vertexId
        #s_in += "check inlinks" + str(u.vertexId)
        for i in range(0, len(u.links)):
            dest = int(u.links[i])
            s_in += str(dest) + ","
            temp= Edge(int(source), int(dest))
            arr_send[int(source) % size].append(temp)
            # need to call rpc to dest worker to do it.  //parallel here
            #arr_send[dest%size]= np.append(arr_send[dest],source)
            #_local_dict[dest].in_links.append(source)  # need to check with saddlebags because dest does not belong to this worker
        #send to other workers to do it.
        for i in range(0,size):
            my_target = "worker" + str(i)
            rpc.rpc_async(my_target, add_outlinks, args=(arr_send[i]))

        #dist.barrier()
        #end sending

        # print(s_in)
    
    '''
    for k in range(0, 2):  # repeat many time :D
        my_result = 0
        for i in range(0, size):
            my_target = "worker" + str(i)
            results = run2(my_target, my_key)
            my_result += results
            print_hello(results, i, rank)  # call rank i print hello results and current rank.
    '''
    rpc.shutdown()  # end_rpc
    #print(my_result)
    print(len(_local_dict), " from rank ", rank)


if __name__ == "__main__":
    size = 2
    processes = []
    for rank in range(size):
        p = Process(target=init_process, args=(rank, size, run))
        p.start()
        processes.append(p)

    for p in processes:
        p.join()